import Vue from 'vue'
import App from '@/Settings/Settings.vue'
import { isView } from 'vueonrails'
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import vSelect from 'vue-select'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-select/dist/vue-select.css'

Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.component('v-select', vSelect)

document.addEventListener('DOMContentLoaded', () => {
  if (isView('settings#show')) {
    document.body.appendChild(document.createElement('settings'))
    const settings = new Vue({
      render: h => h(App)
    }).$mount('settings')
  }
})
