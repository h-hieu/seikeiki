import Vue from 'vue'
import App from '@/MachineStates/MachineState'
import { isView } from 'vueonrails'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

document.addEventListener('DOMContentLoaded', () => {
  if (isView('machine_states#show')) {
    document.body.appendChild(document.createElement('machine_state'))
    const machine_state = new Vue({
      render: h => h(App)
    }).$mount('machine_state')
  }
})
