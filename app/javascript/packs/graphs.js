import Vue from 'vue'
import App from '@/Graphs/Graph.vue'
import { isView } from 'vueonrails'
import BootstrapVue from 'bootstrap-vue'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts/highcharts'
import dataModule from 'highcharts/modules/data'
import boostModule from 'highcharts/modules/boost'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

dataModule(Highcharts)
boostModule(Highcharts)

Vue.use(BootstrapVue)
Vue.use(HighchartsVue)

document.addEventListener('DOMContentLoaded', () => {
  if (isView('graphs#show')) {
    document.body.appendChild(document.createElement('graph'))
    const graph = new Vue({
      render: h => h(App)
    }).$mount('graph')
  }
})
