class SettingForm < BaseForm
  include ActiveModel::Model

  attribute :id, Integer
  attribute :value_id, Integer
  attribute :disp_f, Boolean
  attribute :title, String
  attribute :lower_limit, Float
  attribute :upper_limit, Float
  attribute :scale, Float
  attribute :offset, Float
  attribute :seikeiki_id, Integer
  attribute :product_code, String

  validates :id, presence: true
  validates :value_id, presence: true
  validates :disp_f, inclusion: { in: [true, false] }
  validates :title, presence: true, length: { maximum: 15 }
  validates :lower_limit, presence: true, numericality: { greater_than_or_equal_to: -99_999.999,
                                                          less_than_or_equal_to: ->(setting_form) { setting_form.upper_limit } }
  validates :upper_limit, presence: true, numericality: { greater_than_or_equal_to: ->(setting_form) { setting_form.lower_limit },
                                                          less_than_or_equal_to: 99_999.999 }
  validates :scale, presence: true, numericality: { greater_than_or_equal_to: 1.000, less_than_or_equal_to: 99_999.999 }
  validates :offset, presence: true, numericality: { greater_than_or_equal_to: -99_999.999, less_than_or_equal_to: 99_999.999 }
  validates :seikeiki_id, presence: true
  validates :product_code, presence: true

  def update
    setting_info = SettingInfo.find(id)
    setting_info.update!(attributes.except(:id, :value_id))
    seikeiki_data = SeikeikiDatum.where("datetime >= :datetime AND seikeiki_id = :seikeiki_id AND product_code = :product_code",
                                        datetime: Time.zone.now, seikeiki_id: seikeiki_id, product_code: product_code)
    return if seikeiki_data.empty?

    # rubocop:disable Rails/SkipsModelValidations
    seikeiki_data.update_all("value#{value_id}_lower": lower_limit, "value#{value_id}_upper": upper_limit)
    # rubocop:enable Rails/SkipsModelValidations
  end
end
