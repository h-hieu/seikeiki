class SettingsForm < BaseForm
  attr_accessor :settings

  attribute :setting_infos_attributes, Array

  def setting_infos_attributes=(setting_infos)
    self.settings = setting_infos.map do |setting_info|
      SettingForm.new(setting_info)
    end
  end

  def update
    return unless valid?

    SettingInfo.transaction do
      settings.each(&:update)
    end
  end

  def valid?
    settings.all?(&:valid?)
  end
end
