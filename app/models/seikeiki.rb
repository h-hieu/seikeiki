class Seikeiki < ApplicationRecord
  has_many :seikeiki_data
  has_many :setting_infos

  VALUE_PER_SEIKEIKI_QUANTITY = 10

  def latest_seikeiki_datum
    seikeiki_data.select(:datetime, :seikeiki_id, :value1, :value1_err_f, :value1_upper, :value1_lower,
                         :value2, :value2_err_f, :value2_upper, :value2_lower, :value3, :value3_err_f,
                         :value3_upper, :value3_lower, :value4, :value4_err_f, :value4_upper, :value4_lower,
                         :value5, :value5_err_f, :value5_upper, :value5_lower, :value6, :value6_err_f,
                         :value6_upper, :value6_lower, :value7, :value7_err_f, :value7_upper, :value7_lower, :value8,
                         :value8_err_f, :value8_upper, :value8_lower, :value9, :value9_err_f, :value9_upper,
                         :value9_lower, :value10, :value10_err_f, :value10_upper, :value10_lower, :product_code, :lot_number)
                 .order(datetime: :desc).find_by("datetime <= ?", Time.zone.now)
  end

  def latest_abnormal_data(quantity)
    (1..VALUE_PER_SEIKEIKI_QUANTITY).each.map do |value_id|
      seikeiki_data.where(datetime: Time.zone.now.prev_day..Time.zone.now, "value#{value_id}_err_f".to_sym => true)
                   .select("value#{value_id}", :datetime).order(datetime: :desc).limit(quantity)
    end
  end
end
