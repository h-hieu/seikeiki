class SeikeikiDatum < ApplicationRecord
  belongs_to :seikeiki

  before_create :update_lower_upper

  class << self
    def data_ruby_for_graph
      lastest_datetime_seikeiki = SeikeikiDatum.order(datetime: :desc).first.datetime
      unimported_data = RubyDataForGraph.where("datetime > ?", lastest_datetime_seikeiki)
      create_seikeiki_data(unimported_data)
    end

    def create_seikeiki_data(unimported_data)
      unimported_data.each do |datum|
        SeikeikiDatum.create(datetime: datum.datetime, seikeiki_id: datum.seikeiki, product_code: datum.product_code,
                             lot_number: datum.lot_number, value1: datum.value1, value2: datum.value2, value3: datum.value3,
                             value4: datum.value4, value5: datum.value5, value6: datum.value6, value7: datum.value7,
                             value8: datum.value8, value9: datum.value9, value10: datum.value10)
      end
    end
  end

  private

  def update_lower_upper
    setting_infos = SettingInfo.where(seikeiki_id: seikeiki_id, product_code: product_code)

    setting_infos.each do |setting_info|
      public_send("value#{setting_info.value_id}_upper=", setting_info.upper_limit)
      public_send("value#{setting_info.value_id}_lower=", setting_info.lower_limit)
      public_send("value#{setting_info.value_id}_err_f=", public_send("value#{setting_info.value_id}")
                                          .between?(setting_info.lower_limit, setting_info.upper_limit))
    end
  end
end
