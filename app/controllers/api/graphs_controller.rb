module Api
  class GraphsController < ApplicationController
    layout false

    # rubocop:disable AbcSize
    def show
      return unless params[:seikeiki_id]

      seikeiki = Seikeiki.find(params[:seikeiki_id])
      seikeiki_data = seikeiki.seikeiki_data.where(datetime: Time.zone.now.yesterday..Time.zone.now)
                              .pluck(:datetime, :value1, :value2, :value3, :value4, :value5, :value6, :value7, :value8, :value9,
                                     :value10)

      offset_setting_infos = seikeiki.setting_infos.pluck :offset

      seikeiki_data_for_graph = seikeiki_data.map do |datum|
        values = datum.drop(1)
        max_value = values.max

        calculated_values = values.map do |value|
          (max_value.to_f / value).round * value
        end

        calculated_values_sum_offset = [offset_setting_infos, calculated_values].transpose.map(&:sum)

        [datum[0], calculated_values_sum_offset[0], calculated_values_sum_offset[1], calculated_values_sum_offset[2],
         calculated_values_sum_offset[3], calculated_values_sum_offset[4], calculated_values_sum_offset[5],
         calculated_values_sum_offset[6], calculated_values_sum_offset[7], calculated_values_sum_offset[8],
         calculated_values_sum_offset[9]]
      end

      render json: seikeiki_data_for_graph
    end
    # rubocop:enable AbcSize
  end
end
