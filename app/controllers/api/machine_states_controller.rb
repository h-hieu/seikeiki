module Api
  class MachineStatesController < ApplicationController
    layout false

    VALUE_PER_SEIKEIKI_QUANTITY = 10

    def show
      machine_state_data = Seikeiki.find_each.map do |seikeiki|
        latest_abnormal_data_quantity = 5
        latest_seikeiki_datum = seikeiki.latest_seikeiki_datum

        {
          seikeiki_id: seikeiki.id,
          latest_seikeiki_datum: latest_seikeiki_datum,
          latest_abnormal_data: seikeiki.latest_abnormal_data(latest_abnormal_data_quantity),
          latest_abnormal_data_quantity: latest_abnormal_data_quantity,
          has_abnormal_value: abnormal_value?(latest_seikeiki_datum),
          has_import_error: import_error?(seikeiki)
        }
      end

      render json: machine_state_data
    end

    private

    def abnormal_value?(latest_seikeiki_datum)
      abnormal = false
      (1..VALUE_PER_SEIKEIKI_QUANTITY).each do |value_id|
        next if latest_seikeiki_datum["value#{value_id}_err_f"] == false

        abnormal = true
        break
      end
      abnormal
    end

    def import_error?(seikeiki)
      latest_log = RubyNewestLog.where(seikeiki: seikeiki.id).order(datetime: :desc).first
      latest_log.datetime < Time.zone.now.ago(5.minutes)
    end
  end
end
