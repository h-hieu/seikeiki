module Api
  class SettingsController < ApplicationController
    layout false

    # rubocop:disable Metrics/AbcSize
    def show
      if params[:keyword].present?
        json_data = select_options(params[:keyword])
      elsif params[:seikeiki_id].present? && params[:product_code].present?
        json_data = table_data(params[:seikeiki_id], params[:product_code])
      end
      render json: json_data
    rescue ActiveRecord::RecordNotFound => e
      render json: { error: e.to_s }, status: :not_found
    end
    # rubocop:enable Metrics/AbcSize

    def update
      @form = SettingsForm.new(setting_infos_params)
      message = @form.update ? "Updated successfully" : "Update failed"
      render json: { message: message }
    end

    private

    def select_options(keyword)
      setting_infos = SettingInfo.where("seikeiki_id LIKE :keyword OR LOWER(product_code) LIKE :keyword",
                                        keyword: "%#{keyword}%")
      setting_infos.uniq(&:product_code).map do |setting_info|
        seikeiki_id = setting_info.seikeiki_id
        product_code = setting_info.product_code
        {
          seikeiki_id: seikeiki_id,
          product_code: product_code,
          label: "成型機#{seikeiki_id} - #{product_code}"
        }
      end
    end

    def table_data(seikeiki_id, product_code)
      seikeiki = Seikeiki.find(seikeiki_id)
      seikeiki.setting_infos.where(product_code: product_code)
              .select(:id, :value_id, :disp_f, :title, :lower_limit, :upper_limit, :scale, :offset, :seikeiki_id, :product_code)
    end

    def setting_infos_params
      params.require(:settings).permit(setting_infos_attributes: [:id, :value_id, :disp_f, :title, :lower_limit, :upper_limit,
                                                                  :scale, :offset, :seikeiki_id, :product_code])
    end
  end
end
