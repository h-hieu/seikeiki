module Api
  class ConfigsController < ApplicationController
    layout false

    MINIMUM_INTERVAL_TIME_IN_SECOND = 1
    MAXIMUM_INTERVAL_TIME_IN_SECOND = 60

    def index
      return unless params[:seikeiki_id]

      seikeiki = Seikeiki.find(params[:seikeiki_id])
      setting_infos = seikeiki.setting_infos
      json_data = { latest_seikeiki_datum: seikeiki.latest_seikeiki_datum }

      if valid_interval_time
        json_data[:valid_config] = true
        json_data[:chartOptions] = {
          data: { dataRefreshRate: Settings.interval_time },
          series: series(setting_infos)
        }
        json_data[:disp_fs] = setting_infos.map(&:disp_f)
      else
        json_data[:valid_config] = false
        json_data[:error_message] = "Interval time must be between #{MINIMUM_INTERVAL_TIME_IN_SECOND} and
          #{MAXIMUM_INTERVAL_TIME_IN_SECOND} seconds. Current interval time is #{Settings.interval_time}"
      end

      render json: json_data
    end

    private

    def valid_interval_time
      return false unless Settings.interval_time.is_a? Integer

      Settings.interval_time.between?(MINIMUM_INTERVAL_TIME_IN_SECOND, MAXIMUM_INTERVAL_TIME_IN_SECOND)
    end

    def series(setting_infos)
      setting_infos.map { |setting_info| { name: setting_info.title } }
    end
  end
end
