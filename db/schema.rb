# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_21_040859) do

  create_table "seikeiki_data", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.datetime "datetime", null: false
    t.bigint "seikeiki_id"
    t.string "product_code", limit: 128
    t.string "lot_number", limit: 20
    t.decimal "value1", precision: 8, scale: 3
    t.decimal "value1_upper", precision: 8, scale: 3
    t.decimal "value1_lower", precision: 8, scale: 3
    t.boolean "value1_err_f", default: false
    t.decimal "value2", precision: 8, scale: 3
    t.decimal "value2_upper", precision: 8, scale: 3
    t.decimal "value2_lower", precision: 8, scale: 3
    t.boolean "value2_err_f", default: false
    t.decimal "value3", precision: 8, scale: 3
    t.decimal "value3_upper", precision: 8, scale: 3
    t.decimal "value3_lower", precision: 8, scale: 3
    t.boolean "value3_err_f", default: false
    t.decimal "value4", precision: 8, scale: 3
    t.decimal "value4_upper", precision: 8, scale: 3
    t.decimal "value4_lower", precision: 8, scale: 3
    t.boolean "value4_err_f", default: false
    t.decimal "value5", precision: 8, scale: 3
    t.decimal "value5_upper", precision: 8, scale: 3
    t.decimal "value5_lower", precision: 8, scale: 3
    t.boolean "value5_err_f", default: false
    t.decimal "value6", precision: 8, scale: 3
    t.decimal "value6_upper", precision: 8, scale: 3
    t.decimal "value6_lower", precision: 8, scale: 3
    t.boolean "value6_err_f", default: false
    t.decimal "value7", precision: 8, scale: 3
    t.decimal "value7_upper", precision: 8, scale: 3
    t.decimal "value7_lower", precision: 8, scale: 3
    t.boolean "value7_err_f", default: false
    t.decimal "value8", precision: 8, scale: 3
    t.decimal "value8_upper", precision: 8, scale: 3
    t.decimal "value8_lower", precision: 8, scale: 3
    t.boolean "value8_err_f", default: false
    t.decimal "value9", precision: 8, scale: 3
    t.decimal "value9_upper", precision: 8, scale: 3
    t.decimal "value9_lower", precision: 8, scale: 3
    t.boolean "value9_err_f", default: false
    t.decimal "value10", precision: 8, scale: 3
    t.decimal "value10_upper", precision: 8, scale: 3
    t.decimal "value10_lower", precision: 8, scale: 3
    t.boolean "value10_err_f", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seikeiki_id", "datetime"], name: "index_seikeiki_data_on_seikeiki_id_and_datetime", unique: true
    t.index ["seikeiki_id"], name: "index_seikeiki_data_on_seikeiki_id"
  end

  create_table "seikeikis", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "setting_infos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin", force: :cascade do |t|
    t.bigint "seikeiki_id"
    t.integer "value_id", null: false
    t.boolean "disp_f", default: true, null: false
    t.string "title"
    t.decimal "scale", precision: 8, scale: 3, default: "1.0", null: false
    t.decimal "offset", precision: 8, scale: 3, default: "0.0", null: false
    t.decimal "upper_limit", precision: 8, scale: 3, default: "99999.999", null: false
    t.decimal "lower_limit", precision: 8, scale: 3, default: "-99999.999", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "product_code", null: false
    t.index ["seikeiki_id"], name: "index_setting_infos_on_seikeiki_id"
  end

  add_foreign_key "seikeiki_data", "seikeikis"
  add_foreign_key "setting_infos", "seikeikis"
end
