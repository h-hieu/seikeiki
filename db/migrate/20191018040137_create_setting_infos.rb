class CreateSettingInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :setting_infos do |t|
      t.references :seikeiki, foreign_key: true
      t.integer :value_number, null: false
      t.boolean :disp_f, null: false
      t.string :title
      t.decimal :scale, precision: 8, scale: 3, null: false, default: 1.000
      t.decimal :offset, precision: 8, scale: 3, null: false, default: 0.000
      t.decimal :upper_limit, precision: 8, scale: 3, null: false, default: 99999.999
      t.decimal :lower_limit, precision: 8, scale: 3, null: false, default: -99999.999

      t.timestamps
    end
  end
end
