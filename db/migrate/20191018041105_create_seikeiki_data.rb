class CreateSeikeikiData < ActiveRecord::Migration[5.2]
  def change
    create_table :seikeiki_data do |t|
      t.datetime :datetime, null: false
      t.references :seikeiki, foreign_key: true
      t.string :product_code, limit: 128
      t.string :lot_number, limit: 20
      t.decimal :value1
      t.decimal :value1_upper
      t.decimal :value1_lower
      t.boolean :value1_err_f, default: false
      t.decimal :value2
      t.decimal :value2_upper
      t.decimal :value2_lower
      t.boolean :value2_err_f, default: false
      t.decimal :value3
      t.decimal :value3_upper
      t.decimal :value3_lower
      t.boolean :value3_err_f, default: false
      t.decimal :value4
      t.decimal :value4_upper
      t.decimal :value4_lower
      t.boolean :value4_err_f, default: false
      t.decimal :value5
      t.decimal :value5_upper
      t.decimal :value5_lower
      t.boolean :value5_err_f, default: false
      t.decimal :value6
      t.decimal :value6_upper
      t.decimal :value6_lower
      t.boolean :value6_err_f, default: false
      t.decimal :value7
      t.decimal :value7_upper
      t.decimal :value7_lower
      t.boolean :value7_err_f, default: false
      t.decimal :value8
      t.decimal :value8_upper
      t.decimal :value8_lower
      t.boolean :value8_err_f, default: false
      t.decimal :value9
      t.decimal :value9_upper
      t.decimal :value9_lower
      t.boolean :value9_err_f, default: false
      t.decimal :value10
      t.decimal :value10_upper
      t.decimal :value10_lower
      t.boolean :value10_err_f, default: false

      t.timestamps
    end

    add_index :seikeiki_data, [:seikeiki_id, :datetime], unique: true
  end
end
