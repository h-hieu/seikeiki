class ChangeSeikeikiDatumColumn < ActiveRecord::Migration[5.2]
  def change
    change_table :seikeiki_data, bulk: true do |t|
      t.change :value1, :decimal, precision: 8, scale: 3
      t.change :value1_upper, :decimal, precision: 8, scale: 3
      t.change :value1_lower, :decimal, precision: 8, scale: 3
      t.change :value2, :decimal, precision: 8, scale: 3
      t.change :value2_upper, :decimal, precision: 8, scale: 3
      t.change :value2_lower, :decimal, precision: 8, scale: 3
      t.change :value3, :decimal, precision: 8, scale: 3
      t.change :value3_upper, :decimal, precision: 8, scale: 3
      t.change :value3_lower, :decimal, precision: 8, scale: 3
      t.change :value4, :decimal, precision: 8, scale: 3
      t.change :value4_upper, :decimal, precision: 8, scale: 3
      t.change :value4_lower, :decimal, precision: 8, scale: 3
      t.change :value5, :decimal, precision: 8, scale: 3
      t.change :value5_upper, :decimal, precision: 8, scale: 3
      t.change :value5_lower, :decimal, precision: 8, scale: 3
      t.change :value6, :decimal, precision: 8, scale: 3
      t.change :value6_upper, :decimal, precision: 8, scale: 3
      t.change :value6_lower, :decimal, precision: 8, scale: 3
      t.change :value7, :decimal, precision: 8, scale: 3
      t.change :value7_upper, :decimal, precision: 8, scale: 3
      t.change :value7_lower, :decimal, precision: 8, scale: 3
      t.change :value8, :decimal, precision: 8, scale: 3
      t.change :value8_upper, :decimal, precision: 8, scale: 3
      t.change :value8_lower, :decimal, precision: 8, scale: 3
      t.change :value9, :decimal, precision: 8, scale: 3
      t.change :value9_upper, :decimal, precision: 8, scale: 3
      t.change :value9_lower, :decimal, precision: 8, scale: 3
      t.change :value10, :decimal, precision: 8, scale: 3
      t.change :value10_upper, :decimal, precision: 8, scale: 3
      t.change :value10_lower, :decimal, precision: 8, scale: 3
    end
  end
end
