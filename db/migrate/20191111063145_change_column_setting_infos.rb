class ChangeColumnSettingInfos < ActiveRecord::Migration[5.2]
  def change
    change_table :setting_infos, bulk: true do |t|
      t.column :product_code, :string, null: false
      t.rename :value_number, :value_id
      t.change :disp_f, :boolean, default: true
    end
  end
end
