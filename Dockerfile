FROM ruby:2.6.5

# Install node 10.x with npm and latest yarn
RUN apt-get install curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update
RUN apt-get install nodejs
RUN curl -o- -L https://yarnpkg.com/install.sh | bash

# Install necessary libs
RUN apt-get install -y git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev default-mysql-client

# Install bundler version 2.0.2
RUN gem install bundler -v 2.0.2
RUN gem update --system

# Set up project folder
RUN mkdir /sensor_info_monitoring_system
WORKDIR /sensor_info_monitoring_system

# Set up ENV
ENV LANG C.UTF-8
ENV BUNDLER_VERSION 2.0.2

# Set up Gems
COPY Gemfile /sensor_info_monitoring_system/Gemfile
COPY Gemfile.lock /sensor_info_monitoring_system/Gemfile.lock
RUN bundle install
