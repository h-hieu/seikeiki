source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.6.5"

gem "bootsnap", ">= 1.1.0", require: false
gem "config"
gem "jbuilder", "~> 2.5"
gem "mysql2", ">= 0.4.4", "< 0.6.0"
gem "puma", "~> 3.11"
gem "rails", "~> 5.2.3"
gem "sass-rails", "~> 5.0"
gem "slim-rails"
gem "uglifier", ">= 1.3.0"
gem "virtus"
gem "vueonrails", "~> 1.x"
gem "webpacker", ">= 4.x"
gem "whenever", require: false

group :development, :test do
  gem "awesome_print"
  gem "brakeman"
  gem "i18n-tasks", "~> 0.9.29"
  gem "pry-byebug"
  gem "pry-coolline"
  gem "pry-rails"
  gem "rails_best_practices"
  gem "rubocop", require: false
  gem "rubocop-performance"
  gem "rubocop-rails"
end

group :development do
  gem "bullet"
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "web-console", ">= 3.3.0"
end

group :test do
  gem "capybara", ">= 2.15"
  gem "chromedriver-helper"
  gem "selenium-webdriver"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]
