# SENSOR INFO MONITORING SYSTEM

## Build environment

### Development (Docker)

1. Clone and enter project's folder

   ```console
   git clone # using SSH or HTTPS method
   cd sensor_info_monitoring_system
   ```

2. Config database

   ```console
   cp config/database.yml.example config/database.yml
   ```

   Then change `socket` in _config/database.yml_ appropriate for your Operating System

3. Create MYSQL config file

   ```console
   touch tmp/my.cnf
   ```

4. Build and run docker

   a. Builds, (re)creates, starts, and attaches to containers for a service

   ```console
   docker-compose up
   ```

   b. Install dependencies

   Open new tab in terminal

   ```console
   docker-compose exec web bash
   bundle install
   yarn install
   ```

   c. Generate dummy data. Only run when you need dummy data

   ```console
   rails db:create db:migrate
   rake dummy_data:create
   rails db
   CREATE TABLE ruby_newest_logs(seikeiki INT AUTO_INCREMENT PRIMARY KEY, datetime DATETIME);
   exit
   rails c
   (1..9).each do |seikeiki_id|
   datetime = seikeiki_id.even? ? Time.zone.now.beginning_of_month : Time.zone.now
   RubyNewestLog.create(seikeiki: seikeiki_id, datetime: datetime)
   end
   exit
   ```

   d. Create database. Only run if you didn't generate dummy data

   ```console
   rails db:create db:migrate
   ```

   e. Start rails server

   ```console
   rails s -b 0.0.0.0
   ```

5. When docker is running, open <http://localhost:3001>

6. Recreate `ruby_newest_logs` table's data

   `ruby_newest_logs` table's data only correct for 5 minutes after you create it's data. After 5 minutes, if you want data which showed on machine state screen
   correct again. Run these commands in docker's terminal

   ```console
   rails c
   RubyNewestLog.destroy_all
   (1..9).each do |seikeiki_id|
   datetime = seikeiki_id.even? ? Time.zone.now.beginning_of_month : Time.zone.now
   RubyNewestLog.create(seikeiki: seikeiki_id, datetime: datetime)
   end
   exit
   ```

### Staging

- TO BE CONTINUED

### Production

- TO BE CONTINUED

## Code Quality Assurance

Run each of these commands then fix any problem that appears

```console
rubocop
rails_best_practices .
brakeman
```
