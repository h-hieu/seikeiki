Rails.application.routes.draw do
  mount Vueonrails::Engine, at: "vue"
  root "graphs#show"
  resource :graph, only: :show
  resource :machine_state, only: :show
  resource :setting, only: :show

  namespace :api, defaults: { format: :json } do
    resource :graph, only: :show
    resource :machine_state, only: :show
    resource :setting, only: [:show, :update]
    resources :configs, only: :index
  end
end
