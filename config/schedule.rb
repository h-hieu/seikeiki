# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:

set :output, "log/schedule.log"

require_relative "environment"
set :environment, Rails.env

every 1.minute do
  runner "SeikeikiDatum.data_ruby_for_graph"
end

#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
