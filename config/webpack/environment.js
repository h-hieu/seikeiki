const { environment } = require('@rails/webpacker')
const alias = require('./alias/alias')
const { VueLoaderPlugin } = require('vue-loader')
const vue = require('./loaders/vue')
const pug = require('./loaders/pug')
const webpack = require('webpack')

environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.plugins.prepend(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    Popper: ['popper.js', 'default']
  })
)
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('pug', pug)
environment.config.merge(alias)
module.exports = environment
