unless Rails.env.production?
  # rubocop:disable BlockLength
  namespace :dummy_data do
    desc "Create dummy data for development and / or testing purpose"
    task create: :environment do
      task_started_at = Time.zone.now
      puts "Task started at #{task_started_at}"

      SEIKEIKI_QUANTITY = 9
      VALUE_PER_SEIKEIKI_QUANTITY = 10

      ONE_DAY_AGO = Time.zone.now.prev_day
      ONE_DAY_LATER = Time.zone.now.next_day
      FETCH_DATA_INTERVAL = 15.seconds
      FETCH_TIMES_BETWEEN = ((ONE_DAY_LATER - ONE_DAY_AGO) / FETCH_DATA_INTERVAL).round

      SEIKEIKI_QUANTITY.times do |i|
        puts "Start creating seikeiki ##{i + 1}"
        seikeiki = Seikeiki.create
        puts "Finish creating seikeiki ##{i + 1}"

        puts "-" * 50

        puts "Start creating setting_infos for seikeiki ##{i + 1}"
        random_disp_f = [true, false]

        # Product codes and lot numbers for seikeiki 3 and 5 are provided by the customer
        if seikeiki.id == 3
          product_code = "BON-MD-20023"
          lot_number = "191023-135"
        elsif seikeiki.id == 5
          product_code = "MLD-MD-20035"
          lot_number = "IN.191023-014"
        else
          product_code = ""
          2.times do
            3.times do |n|
              product_code << ("A".."Z").to_a.sample
              product_code << "-" if n == 2
            end
          end
          product_code << rand(10_000..99_999).to_s

          lot_number = "191031-"
          lot_number << rand(100..999).to_s
        end

        lower_limit = 1000
        upper_limit = 1100

        VALUE_PER_SEIKEIKI_QUANTITY.times do |j|
          puts "Start creating setting_info for value ##{j + 1} of seikeiki ##{i + 1}"
          seikeiki.setting_infos.create(
            product_code: product_code,
            value_id: j + 1,
            disp_f: random_disp_f.sample,
            title: "値#{j + 1}のデータ",
            scale: Settings.initial_data.scale,
            offset: Settings.initial_data.offset,
            upper_limit: upper_limit,
            lower_limit: lower_limit
          )
          puts "Finish creating setting_info for value ##{j + 1} of seikeiki ##{i + 1}"
        end
        puts "Finish creating setting_infos for seikeiki ##{i + 1}"

        puts "-" * 50

        puts "Start creating #{FETCH_TIMES_BETWEEN} seikeiki_data for seikeiki ##{i + 1}"
        FETCH_TIMES_BETWEEN.times do |k|
          puts "Start creating seikeiki_datum ##{k + 1}"
          values_data = []
          VALUE_PER_SEIKEIKI_QUANTITY.times do |l|
            value_upper = upper_limit - l * 100
            value_lower = lower_limit - l * 100
            upper_for_error = value_upper + SEIKEIKI_QUANTITY
            lower_for_error = value_lower - SEIKEIKI_QUANTITY
            value = rand(lower_for_error..upper_for_error)
            value_err_f = !value.between?(value_lower, value_upper)

            value_data = [value, value_upper, value_lower, value_err_f]
            values_data.push(value_data)
          end

          seikeiki.seikeiki_data.create(
            datetime: ONE_DAY_AGO + FETCH_DATA_INTERVAL * k,
            product_code: product_code,
            lot_number: lot_number,
            value1: values_data[0][0],
            value1_upper: values_data[0][1],
            value1_lower: values_data[0][2],
            value1_err_f: values_data[0][3],
            value2: values_data[1][0],
            value2_upper: values_data[1][1],
            value2_lower: values_data[1][2],
            value2_err_f: values_data[1][3],
            value3: values_data[2][0],
            value3_upper: values_data[2][1],
            value3_lower: values_data[2][2],
            value3_err_f: values_data[2][3],
            value4: values_data[3][0],
            value4_upper: values_data[3][1],
            value4_lower: values_data[3][2],
            value4_err_f: values_data[3][3],
            value5: values_data[4][0],
            value5_upper: values_data[4][1],
            value5_lower: values_data[4][2],
            value5_err_f: values_data[4][3],
            value6: values_data[5][0],
            value6_upper: values_data[5][1],
            value6_lower: values_data[5][2],
            value6_err_f: values_data[5][3],
            value7: values_data[6][0],
            value7_upper: values_data[6][1],
            value7_lower: values_data[6][2],
            value7_err_f: values_data[6][3],
            value8: values_data[7][0],
            value8_upper: values_data[7][1],
            value8_lower: values_data[7][2],
            value8_err_f: values_data[7][3],
            value9: values_data[8][0],
            value9_upper: values_data[8][1],
            value9_lower: values_data[8][2],
            value9_err_f: values_data[8][3],
            value10: values_data[9][0],
            value10_upper: values_data[9][1],
            value10_lower: values_data[9][2],
            value10_err_f: values_data[9][3]
          )
          puts "Finish creating seikeiki_datum ##{k + 1}"

          puts "-" * 50
        end
        puts "Finish creating #{FETCH_TIMES_BETWEEN} seikeiki_data for seikeiki ##{i + 1}"
      end
      task_ended_at = Time.zone.now
      puts "Task ended at #{task_ended_at}"
      puts "Task ran in about #{((task_ended_at - task_started_at) / 60).round} minutes"
    end
  end
  # rubocop:enable BlockLength
end
